# Documentação Aplicativo - Eficiência Operacional

## Configuração do Servidor

No servidor SQL Server,  criar a seguinte tabela no banco de dados (teste ou produção).

```mssql
CREATE TABLE tb_app_eficiencia(
	codigo 			INTEGER NOT NULL IDENTITY PRIMARY KEY,
	data_inicial	VARCHAR(MAX) NOT NULL,
	hora_inicial	VARCHAR(MAX) NOT NULL,
	id_recurso		VARCHAR(MAX) NOT NULL,
	recurso			VARCHAR(MAX) NOT NULL,
	ccusto			VARCHAR(MAX) NOT NULL,
	id_operador		VARCHAR(MAX) NOT NULL,
	operador		VARCHAR(MAX) NOT NULL,
	medicao_inicial	INTEGER NOT NULL,
	usuario			VARCHAR(MAX) NOT NULL
)
```

Após criação da tabela, realizar o `git clone` do projeto da API em um computador que funcionará como o servidor da aplicação.

Link do projeto: `https://gitlab.com/dyhalmeida/api-ionic-cata.git`

```bash
# Exemplo
git clone https://gitlab.com/dyhalmeida/api-ionic-cata.git
```

Após realizar o clone, entrar na raiz da pasta do projeto clonado e fazer a instalação das dependências com os seguintes comandos:

```bash
npm install
# ou
npm update
```

O próximo passo é configurar a API para enxegar o servidor SQL Server. Na pasta do projeto, navegar até o arquivo `config.js` e realizar as modificações.

```javascript
// Exemplo de configuração
exports.dbConfig = {
    user: "sa",
    password: "102010",
    server: "10.10.0.4\\GRUPOCATA",
    database: "DADOSTESTE12",
    port: 1320
}

exports.webPort = 9000;

exports.httpMsgFormat = "JSON";

```

Após configuração do servidor sql server, configurar qual tabela sera utilizada no arquivo `crud/crud.js`. A configuração encontra-se no topo.

```javascript
var db = require("../servico/db");
var httpMsg = require("../servico/httpMsg");
var util = require("util");

// Exemplo de configuração da tabela. Essa configuração terá o mesmo nome da tabela criada no inicio dessa documentação.
var tabela = "tb_app_eficiencia";

```

O útimo passo é rodar o servidor node na raiz do projeto, executando o arquivo `app.js`.

```bash
# Exemplo
node app.js
```

O servidor da API deverá ficar na escuta.



# Configuração do aplicativo no tablet

Após as configurações do servidor da API, com o aplicativo instalado no tablet, logar com o usuário `admin` e senha `toor`.

Na tela a seguir, criar um usuário com o perfil de `user` para ter acesso aos apontamentos e realizar a configuração do ip para qual o aplicativo irá se conectar.

> **O ip configurado deverá ser o ip da máquina que está rodando o servidor API**

Ao logar-se com o usuário de perfil **user** poderá ocorrer erro de conexão na primeira tentativa de acesso, basta tentar loga-se novamente.