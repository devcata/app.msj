// import nativo
import { Injectable } from '@angular/core';

// import de usuário
import { SQLiteObject } from '@ionic-native/sqlite';
//===========================================================
import { DatabaseProvider } from './../database/database';
//===========================================================


@Injectable()
export class ImprodutivoProvider {

  constructor(private _database: DatabaseProvider) { }

  //Método p/ inserir custo
  public insertImpro_A(improdutivo) {
    return this._database.createDataBase()
      .then((db: SQLiteObject) => {
        //console.log(`GRAVOU ${JSON.stringify(improdutivo)}`);
        let sql = 'INSERT INTO impro_A (dataInicial, dataFinal, horaInicial, horaFinal, ccusto, desccusto, idmotivo, motivo) values (?,?,?,?,?,?,?,?)';
        let data = [improdutivo.dataInicial, improdutivo.dataFinal, improdutivo.horaInicial, improdutivo.horaFinal, improdutivo.ccusto, improdutivo.desccusto, improdutivo.idMotivo, improdutivo.motivo];
        return db.executeSql(sql, data).catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public selectLastIdImpro_A() {
    return this._database.createDataBase()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT id FROM impro_A ORDER BY id DESC LIMIT 1';

        return db.executeSql(sql, [])
          .then((data: any) => {
            return data.rows.item(0).id;
          }).catch((e) => console.error(e)); // fim db.executeSql()
      }).catch((e) => console.error(JSON.stringify(e))); // fim _database.createDataBase()
  }

  //Método p/ inserir custo
  public async insertImpro_B(recurso) {

    let id;
    await this.selectLastIdImpro_A().then(data => {
      id = data;
    });
    //console.log(`id do imprd_A ${id}`);
    return this._database.createDataBase()
      .then((db: SQLiteObject) => {
        //console.log(`GRAVOU ${JSON.stringify(recurso)}`);
        let sql = 'INSERT INTO impro_B (id, recurso, descricao) values (?, ?, ?)';
        let data = [id, recurso.codigo, recurso.descricao];
        return db.executeSql(sql, data).catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  //Método p/ selecionar todos os apontamentos
  public async selectAllImpro_A(filtro?: string) {
    return await this._database.createDataBase()
      .then(async (db: SQLiteObject) => {

        //let sql = "SELECT a.id id_A, a.dataInicial, a.dataFinal, a.horaInicial, a.horaFinal, a.ccusto, a.motivo, b.id id_B, b.recurso, b.descricao";
        //sql += " FROM impro_A a INNER JOIN impro_B b ON a.id = b.id";
        let sql = "SELECT id, dataInicial, dataFinal, horaInicial, horaFinal, ccusto, desccusto, idmotivo, motivo FROM impro_A";
        let data = [];

        if (filtro) {
          //sql += " WHERE id_operador LIKE ?";
          //data.push(filtro);
        }
        //console.log(sql.toString());
        return await db.executeSql(sql, data)
          .then((data: any) => {
            //console.log(data.rows.length > 0);
            if (data.rows.length > 0) {
              let improdutivos: any[] = [];
              for (let i = 0; i < data.rows.length; i++) {
                let improdutivo = data.rows.item(i);
                improdutivos.push(improdutivo);
              }
              return improdutivos;
            }
            return [];
          }).catch((e) => console.error(JSON.stringify(e)));
      }).catch((e) => console.error(JSON.stringify(e)));
  }

  //Método p/ selecionar todos os apontamentos
  public async selectAllImpro_B(id) {
    return await this._database.createDataBase()
      .then(async (db: SQLiteObject) => {

        //let sql = "SELECT a.id id_A, a.dataInicial, a.dataFinal, a.horaInicial, a.horaFinal, a.ccusto, a.motivo, b.id id_B, b.recurso, b.descricao";
        //sql += " FROM impro_A a INNER JOIN impro_B b ON a.id = b.id";
        let sql = "SELECT id, recurso, descricao FROM impro_B WHERE id = ?";
        let data = [id];

        //console.log(sql.toString());
        return await db.executeSql(sql, data)
          .then((data: any) => {
            //console.log(data.rows.length > 0);
            if (data.rows.length > 0) {
              let recursos: any[] = [];
              for (let i = 0; i < data.rows.length; i++) {
                let recurso = data.rows.item(i);
                recursos.push(recurso);
              }
              return recursos;
            }
            return [];
          }).catch((e) => console.error(JSON.stringify(e)));
      }).catch((e) => console.error(JSON.stringify(e)));
  }

  //Método p/ atualizar um usuário
  public updateImpro_A(dataFinal, horaFinal, id) {
    return this._database.createDataBase()
      .then((db: SQLiteObject) => {

        let sql = 'UPDATE impro_A SET dataFinal = ?, horaFinal = ? WHERE id = ?';
        let data = [dataFinal, horaFinal, id];

        return db.executeSql(sql, data).catch((e) => console.error(e));

      }).catch((e) => console.error(e));
  }

  //Método p/ deletar um apontamento
  public deleteImpro_A(id) {
    return new Promise((resolve, reject) => {
      return this._database.createDataBase()
        .then((db: SQLiteObject) => {

          let sql = "DELETE FROM impro_A WHERE id = ?";
          let data = [id];

          return db.executeSql(sql, data).then((data: any) => resolve(true)).catch((e) => reject(e));

        }).catch((e) => reject(e));
    });
  }

  //Método p/ deletar um apontamento
  public deleteImpro_B(id) {
    return new Promise((resolve, reject) => {
      return this._database.createDataBase()
        .then((db: SQLiteObject) => {

          let sql = "DELETE FROM impro_B WHERE id = ?";
          let data = [id];

          return db.executeSql(sql, data).then((data: any) => resolve(true)).catch((e) => reject(e));

        }).catch((e) => reject(e));
    });
  }
}
