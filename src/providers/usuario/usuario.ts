// import de nativo
import { Injectable } from '@angular/core';

// import de usuário
import { SQLiteObject } from '@ionic-native/sqlite';
//==========================================================
import { DatabaseProvider } from './../database/database';
//==========================================================
import { Usuario } from './../../model/usuario.model';
//==========================================================
import { StringBuilder } from '../../utils/stringBuilder.utils';
// fim import de usuário

@Injectable()
export class UsuarioProvider {

  constructor(private _database : DatabaseProvider) {}

  //Método p/ inserir usuário
  public insertUsuario(usuario : Usuario) 
  {
    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'INSERT INTO usuario (login, senha, perfil) VALUES (?,?,?)';
      let data = [usuario.getLogin(), usuario.getSenha(), usuario.getPerfil()];

      return db.executeSql(sql, data).catch((e) => console.error(e));
    }).catch((e) => console.error(e));
  }

  //Método p/ selecionar apenas um usuário
  public selectUsuario(login : string, senha : string) 
  {
    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'SELECT * FROM usuario WHERE login = ? AND senha = ?';
      let data = [login, senha];

      return db.executeSql(sql, data)
      .then((data: any) => {

        if (data.rows.length > 0) {
          let registro = data.rows.item(0);
          let usuario = new Usuario();
    
          usuario.setId(registro.id);
          usuario.setLogin(registro.login);
          usuario.setSenha(registro.senha);
          usuario.setPerfil(registro.perfil);
          return usuario;
        } // fim if()
        return false;

      }).catch((e) => console.error('ERROR' + e)); // fim db.executeSql()

    }).catch((e) => console.error('ERROR' + e)); // fim _database.createDataBase()
  }

  //Método apenas para selecionar usuário pelo seu ID.
  public selectUsuarioByID(id : number) 
  {
    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'SELECT * FROM usuario WHERE id = ?';
      let data = [id];

      return db.executeSql(sql, data)
      .then((data: any) => {

        if (data.rows.length > 0) {
          let registro = data.rows.item(0);
          let usuario = new Usuario();

          usuario.setId(registro.id);
          usuario.setLogin(registro.login);
          usuario.setSenha(registro.senha);
          usuario.setPerfil(registro.perfil);
          return usuario;
        } // fim if()

        return false;

      }).catch((e) => console.error('ERROR' + e)); // fim db.executeSql()

    }).catch((e) => console.error('ERROR' + e)); // fim _database.createDataBase()
  }

  //Método p/ selecionar todos os usuários
  public selectAllUsuario(login? : string) 
  {
    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = new StringBuilder();
      sql.append('SELECT * FROM usuario');
      let data = [];

      if (login) {
        sql.append(' WHERE login LIKE ?');
        data.push(login)
      }

      sql.append(' ORDER BY login DESC');
      //console.log(sql.toString());
      

      return db.executeSql(sql.toString(), data)
      .then((data: any) => {

        if (data.rows.length > 0) {
          let usuarios: any[] = [];
          for (let i = 0; i < data.rows.length; i++) {
            let user = data.rows.item(i);
            usuarios.push(user);
          }
          return usuarios;
        }
        return [];
      }).catch((e) => console.error(e)); // fim db.executeSql()

    }).catch((e) => console.error(e)); // fim _database.createDataBase()
  }

  //Método p/ deletar um usuário
  public deleteUsuario(id : number) 
  {
    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'DELETE FROM usuario WHERE id = ?';
      let data = [id];

      return db.executeSql(sql, data).catch((e) => console.error(e));

    }).catch((e) => console.error(e));
  }

  //Método p/ atualizar um usuário
  public updateUsuario(usuario : Usuario) 
  {
    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'UPDATE usuario SET login = ?, senha = ?, perfil = ? WHERE id = ?';
      let data = [usuario.getLogin(), usuario.getSenha(), usuario.getPerfil(), usuario.getId()];

      return db.executeSql(sql, data).catch((e) => console.error(e));

    }).catch((e) => console.error(e));
  }

}
