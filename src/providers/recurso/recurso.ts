// import nativo
import { Injectable } from '@angular/core';

// import de usuário
import { SQLiteObject } from '@ionic-native/sqlite';
//==========================================================================
import { DatabaseProvider } from './../database/database';
//==========================================================================
//import { Recurso } from './../../model/recurso.model';
// fim import de usuário

@Injectable()
export class RecursoProvider {

  constructor(private _database : DatabaseProvider) {}

  //Método p/ inserir recursos
  public insertRecurso(recurso: any) 
  {
    //console.log("Inserindo Recurso: " + codigo + " - " + descricao + " - " + ccusto);

    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'INSERT INTO recursos (codigo, filial, descricao, ccusto) VALUES (?,?,?,?)';
      let data = [recurso.codigo, recurso.filial, recurso.descricao, recurso.ccusto];

      return db.executeSql(sql, data).catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  //Método p/ selecionar todos os recursos
  public selectAllRecurso(filtro : string) 
  {
    let ret: any = this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'SELECT * FROM recursos WHERE codigo LIKE ? OR descricao LIKE ?';
      let data = [filtro + '%', filtro + '%'];

      return db.executeSql(sql, data)
      .then((data: any) => {

        if (data.rows.length > 0) {

          let recursos: any[] = [];

          for (let i = 0; i < data.rows.length; i++) {
            let recurso = data.rows.item(i);
            recursos.push(recurso);
          } // fim for()

          return recursos;
        } // fim if()
        return [];
      }).catch((e) => console.error(e)); // fim db.executeSql()

    }).catch((e) => console.error(e)); // fim _database.createDataBase()

    return ret;
  }
}