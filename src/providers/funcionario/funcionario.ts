// import nativo
import { Injectable } from '@angular/core';

// import de usuário
import { SQLiteObject } from '@ionic-native/sqlite';
//===========================================================
import { DatabaseProvider } from './../database/database';
//===========================================================
//import { Funcionario } from './../../model/funcionario.model';
// fim import de usuário

@Injectable()
export class FuncionarioProvider {

  constructor(private _database : DatabaseProvider) {}

  //Método p/ inserir funcionários
  public insertFuncionario(funcionario: any) 
  {
    //console.log("Inserindo funcionário: " + matricula + " - " + nome);
    return this._database.createDataBase()

    .then((db: SQLiteObject) => {

        let sql = 'INSERT INTO funcionarios (matricula, filial, nome) VALUES (?,?,?)';
        let data = [funcionario.matricula, funcionario.filial, funcionario.nome];

        return db.executeSql(sql, data).catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }


  //Método p/ selecionar todos os funcionários
  public selectAllFuncionario(filtro: string) 
  {
    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'SELECT * FROM funcionarios WHERE matricula LIKE ? OR nome LIKE ?';
      let data = [filtro + '%', filtro + '%'];

      return db.executeSql(sql, data)
      .then((data: any) => {
        //console.log(data.rows.length > 0);
        if (data.rows.length > 0) {

          let funcionarios: any[] = [];

          for (let i = 0; i < data.rows.length; i++) {
            let funcionario = data.rows.item(i);
            funcionarios.push(funcionario);
          } // fim for()
          return funcionarios;
        } // fim if()
        return [];
      })
      .catch((e) => console.error(e));
      // fim db.executeSql()
    })
    .catch((e) => console.error(e));
    // fim _database.createDatabe()
  }
}
