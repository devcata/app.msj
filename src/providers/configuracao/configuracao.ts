// import nativo
import { Injectable } from '@angular/core';

// import de usuário
import { SQLiteObject } from '@ionic-native/sqlite';
//==========================================================
import { DatabaseProvider } from './../database/database';
//==========================================================
import { Configuracao } from './../../model/configuracao.model';
// fim import de usuário

@Injectable()
export class ConfiguracaoProvider {

  constructor(private _database : DatabaseProvider) {}

  //Método p/ selecionar IP
  public getConfiguracao() 
  {
    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'SELECT * FROM configuracao';

      return db.executeSql(sql, [])
      .then((data: any) => {

        if (data.rows.length > 0) {

          let registro = data.rows.item(0);
          let configuracao = new Configuracao();

              configuracao.setCodigo(registro.codigo);
              //console.log('selectIP() ' + configuracao.getCodigo());
              
              configuracao.setIp(registro.ip);
              console.log(`IP: ${configuracao.getIp()}`);
              
              configuracao.setPorta(registro.porta);
              console.log(`PORTA: ${configuracao.getPorta()}`);
            
              return configuracao;
        } // fim if()

        return false;

      }).catch((e) => console.error('ERROR' + e)); // fim db.executeSql()

    }).catch((e) => console.error('ERROR' + e)); // fim _database.createDataBase()
  }

  //Método p/ atualizar o IP
  public updateConfiguracao(configuracao : Configuracao): Promise<any>
  {
    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'UPDATE configuracao SET ip = ?, porta = ? WHERE codigo = ?';
      let data = [configuracao.getIp(), configuracao.getPorta(), configuracao.getCodigo()];

      return db.executeSql(sql, data).catch((e) => console.error(e));

    }).catch((e) => console.error(e));
  }

}
