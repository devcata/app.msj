// import nativo
import { Injectable } from '@angular/core';

// import de usuário
import { SQLiteObject } from '@ionic-native/sqlite';
//===========================================================
import { DatabaseProvider } from './../database/database';
//===========================================================

@Injectable()
export class MotivoProvider {

  constructor(private _database: DatabaseProvider) { }

  //Método p/ inserir custo
  public insertMotivo(motivo) {
    return this._database.createDataBase()
      .then((db: SQLiteObject) => {
        let sql = 'INSERT INTO motivos (filial, ccusto, motivo, descricao) VALUES (?,?,?,?)';
        let data = [motivo.filial, motivo.ccusto, motivo.motivo, motivo.descricao];
        return db.executeSql(sql, data).catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  //Método p/ selecionar todos os recursos
  public selectAllMotivo(filtro: string, ccusto: string) {

    return this._database.createDataBase()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM motivos WHERE (motivo LIKE ? OR descricao LIKE ?) AND ccusto = ?';
        let data = [filtro + '%', filtro + '%', ccusto];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let motivos: any[] = [];
              for (let i = 0; i < data.rows.length; i++) {
                motivos.push(data.rows.item(i));
              } // fim for()
              return motivos;
            } // fim if()
            return [];
          }).catch((e) => console.error(e)); // fim db.executeSql()
      }).catch((e) => console.error(e)); // fim _database.createDataBase()
  }

}
