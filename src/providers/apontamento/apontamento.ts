// import nativo
import { Injectable } from '@angular/core';

// import de usuário
import { SQLiteObject } from '@ionic-native/sqlite';
//==========================================================
import { DatabaseProvider } from './../database/database';
//==========================================================
import { Apontamento } from './../../model/apontamento.model';
//==========================================================
import { StringBuilder } from './../../utils/stringBuilder.utils';
// fim import de usuário

@Injectable()
export class ApontamentoProvider {

  constructor(private _database : DatabaseProvider) {}

  //Método p/ inserir apontamento
  public insertApontamento(apontamento : Apontamento) 
  {
    //console.log('provider apontamento: ' + apontamento.data_inicial);

    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = new StringBuilder();
      sql.append('INSERT INTO apontamento ');
      sql.append('(id_recurso, recurso, ccusto, id_operador, operador, medicao_inicial, turma, usuario)');
      sql.append('VALUES (?,?,?,?,?,?,?,?)');

      console.log("========LOGS==========",
        apontamento.getIdRecurso(),
        apontamento.getRecurso(), 
        apontamento.getCCusto(), 
        apontamento.getIdOperador(), 
        apontamento.getOperador(), 
        apontamento.getMedicaoInicial()
        //apontamento.getUsuario()
      );

      let data = [
        apontamento.getIdRecurso(), 
        apontamento.getRecurso(), 
        apontamento.getCCusto(), 
        apontamento.getIdOperador(), 
        apontamento.getOperador(), 
        apontamento.getMedicaoInicial(), 
        apontamento.getTurma(),
        apontamento.getUsuario()
      ];

      console.log(sql.toString());
      

      return db.executeSql(sql.toString(), data).catch((e) => console.error(e));

    }).catch((e) => console.error(e));
  }

  //Método p/ selecionar todos os apontamentos
  public async selectAllApontamento(filtro?: string) 
  {
    return await this._database.createDataBase()
    .then(async (db: SQLiteObject) => {

      let sql = new StringBuilder();
      sql.append("SELECT * FROM apontamento");
      let data = [];

      if (filtro) {
        sql.append(" WHERE id_operador LIKE ?");
        data.push(filtro);
      }

      //console.log(sql.toString());
      
      return await db.executeSql(sql.toString(), data)
      .then((data: any) => {

        //console.log(data.rows.length > 0);

        if (data.rows.length > 0) 
        {
          let apontamentos: any[] = [];
          for (let i = 0; i < data.rows.length; i++) {
            let apontamento = data.rows.item(i);
            apontamentos.push(apontamento);
          }
          return apontamentos;
        }
        return [];
      }).catch((e) => console.error(e));
    }).catch((e) => console.error(e));
  }

  //Método p/ deletar um apontamento
  deleteApontamento(codigo: number) 
  {
    return new Promise((resolve, reject) => 
    {
     return this._database.createDataBase()
     .then((db: SQLiteObject) => {

        let sql = "DELETE FROM apontamento WHERE codigo = ?";
        let data = [codigo];

        return db.executeSql(sql, data).then((data: any) => resolve(true)).catch((e) => reject(e));

      }).catch((e) => reject(e));
    });
  }

  //Método para deletar todos os apontamentos
  public deleteAllApontamentos() 
  {
    return this._database.createDataBase()
    .then((db: SQLiteObject) => {

      let sql = 'DELETE FROM apontamento';
      return db.executeSql(sql, []).catch((e) => console.error(e));

    }).catch((e) => console.error(e));
  }

}
