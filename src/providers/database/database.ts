// import nativo
import { Injectable } from '@angular/core';

// import de usuário
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {

  constructor(private sqlite: SQLite) 
  {

  }

  // Método p/ criar arquivo.db
  public createDataBase() 
  {
    return this.sqlite.create({
      name: 'gc.db',
      location: 'default'
    });
  }

  //Métodos para a criação das tabelas
  private createTables(db: SQLiteObject) 
  {
    db.sqlBatch([
      ["DROP TABLE IF EXISTS recursos"],
      ["DROP TABLE IF EXISTS funcionarios"],
      ["DROP TABLE IF EXISTS custos"],
      ["DROP TABLE IF EXISTS motivos"],
      //["DROP TABLE IF EXISTS impro_A"],
      //["DROP TABLE IF EXISTS impro_B"],
      //["DROP TABLE IF EXISTS apontamento"],
      ["CREATE TABLE IF NOT EXISTS usuario (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, login TEXT UNIQUE, senha TEXT, perfil TEXT)"],
      ["CREATE TABLE IF NOT EXISTS recursos (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, codigo TEXT not null, filial TEXT NOT NULL, descricao TEXT NOT NULL, ccusto TEXT NOT NULL)"],
      ["CREATE TABLE IF NOT EXISTS funcionarios (codigo INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, matricula TEXT NOT NULL, filial TEXT NOT NULL, nome TEXT NOT NULL)"],
      ["CREATE TABLE IF NOT EXISTS apontamento (codigo INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, data_inicial TEXT default(date()), hora_inicial TEXT default(time('now','localtime')), id_recurso TEXT NOT NULL, recurso TEXT NOT NULL, ccusto TEXT NOT NULL, id_operador TEXT NOT NULL, operador TEXT NOT NULL, medicao_inicial TEXT NOT NULL, turma VARCHAR(1) NOT NULL, usuario VARCHAR(6) NOT NULL)"],
      ["CREATE TABLE IF NOT EXISTS configuracao (codigo INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ip TEXT NOT NULL, porta TEXT NOT NULL)"],
      ["CREATE TABLE IF NOT EXISTS custos (codigo INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ccusto TEXT NOT NULL, descricao TEXT NOT NULL)"],
      ["CREATE TABLE IF NOT EXISTS motivos (codigo INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, filial TEXT NOT NULL, ccusto TEXT NOT NULL, motivo TEXT NOT NULL, descricao TEXT NOT NULL)"],
      ["CREATE TABLE IF NOT EXISTS impro_A (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, dataInicial TEXT NOT NULL, dataFinal TEXT NULL, horaInicial TEXT NOT NULL, horaFinal TEXT NULL, ccusto TEXT NOT NULL, desccusto TEXT NOT NULL, idmotivo TEXT NOT NULL, motivo TEXT NOT NULL)"],
      ["CREATE TABLE IF NOT EXISTS impro_B (id INTEGER NOT NULL, recurso TEXT NOT NULL, descricao TEXT NOT NULL)"]
    ])
      .then(() => console.log('Tabelas criadas com sucesso'))
      .catch(e => console.log('Erro ao criar as tabelas', e));
  }

  //Método para inserir usuário admin
  private insertDefaultUser(db: SQLiteObject) 
  {
    db.executeSql('SELECT COUNT(id) as qtd FROM usuario', {})
      .then((data: any) => {

        //Se não existe nenhum registro
        if (data.rows.item(0).qtd == 0) {
          // Inserindo usuário admin
          db.sqlBatch([
            ['INSERT INTO usuario (login, senha, perfil) VALUES (?,?,?)', ['101001', '101001', 'admin']]
          ])
            .then(() => console.log('usuário padrão incluído'))
            .catch(e => console.error('Erro ao incluir usuário padrão', e));
        }

      })
      .catch(e => console.error('Erro ao consultar a qtd de usuários', e));
  }

  //Método para inserir IP+Porta default
  private insertDefaultIP(db : SQLiteObject) 
  {
    db.executeSql('SELECT COUNT(codigo) as qtd FROM configuracao', {})
      .then((data : any) => {

        //Se não existe nenhum registro
        if (data.rows.item(0).qtd == 0) {
          // Inserindo usuário admin
          db.sqlBatch([
            ['INSERT INTO configuracao (ip, porta) VALUES (?,?)', ['0.0.0.0', '9000']]
          ])
            .then(() => console.log('Ip padrão incluído'))
            .catch(e => console.error('Erro ao incluir ip padrão', e));
        }
      })
      .catch(e => console.error('Erro ao consultar a qtd de ips', e));
  }

  //Método para a criação do banco de dados
  public create() {
    return this.createDataBase()
     .then((db: SQLiteObject) => {

        this.createTables(db);
        this.insertDefaultUser(db);
        this.insertDefaultIP(db);
  
      })
      .catch(e => console.error(e));
  }

}
