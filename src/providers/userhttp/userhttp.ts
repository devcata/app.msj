// import nativo
import { Injectable } from '@angular/core';

// import de usuário
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
//===================================================================================
//import { ConfiguracaoProvider } from './../../providers/configuracao/configuracao';
//===================================================================================
import { Recurso } from './../../model/recurso.model';
import { Funcionario } from './../../model/funcionario.model';
import { Configuracao } from './../../model/configuracao.model';
// fim import de usuário

@Injectable()
export class UserhttpProvider {

  constructor(private _http: Http) { }

  //Método GET de recursos
  public getRecursos(ip: string, porta: string) {

    let url = `http://${ip}:${porta}/recursos`;
    return this._http.get(url)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  //Método GET de funcionários
  public getFuncionarios(ip: string, porta: string) {

    let url = `http://${ip}:${porta}/operadores`;
    return this._http.get(url).map((res: Response) => res.json())
      .catch(this.handleError);
  }

  //Método GET de custos
  public getCustos(ip: string, porta: string) {

    let url = `http://${ip}:${porta}/custos`;
    return this._http.get(url).map((res: Response) => res.json())
      .catch(this.handleError);
  }

  //Método GET de custos
  public getMotivos(ip: string, porta: string) {

    let url = `http://${ip}:${porta}/motivos`;
    return this._http.get(url).map((res: Response) => res.json())
      .catch(this.handleError);
  }

  //Método POST de apontamento de eficiência operacional
  public postApontamento(apontamento: any, ip: string, porta: string) {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let url = `http://${ip}:${porta}/eficiencia-msj`;

    return new Promise((resolve, reject) => {
      this._http.post(url, JSON.stringify(apontamento), options)
        .subscribe(res => {
          let objJson = JSON.parse(JSON.stringify(res));
          //console.log(objJSON.status);
          resolve(objJson.status);
        }, err => {
          let objJsonErro = JSON.parse(JSON.stringify(err));
          reject(objJsonErro.status);
        }
        )
    });
  }

  //Método POST de apontamento de eficiência operacional
  public postImprodutivo(improdutivo: any, ip: string, porta: string) {

    //console.log('POST-IMPRODUTIVO');

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let url = `http://${ip}:${porta}/improdutivo-msj`;

    return new Promise((resolve, reject) => {

      this._http.post(url, JSON.stringify(improdutivo), options)
        .subscribe(res => {
          resolve('send');
        }, err => {
          reject(err);
        }
        )
    });
  }

  private handleError(error: Response) {
    //console.error(/*JSON.stringify(error)*/error);
    return Observable.throw(error);
  }

}