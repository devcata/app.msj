// import nativo
import { Injectable } from '@angular/core';

// import de usuário
import { SQLiteObject } from '@ionic-native/sqlite';
//===========================================================
import { DatabaseProvider } from './../database/database';
//===========================================================

@Injectable()
export class CustoProvider {

  constructor(private _database: DatabaseProvider) { }

  //Método p/ inserir custo
  public insertCusto(custo) {
    return this._database.createDataBase()
      .then((db: SQLiteObject) => {
        let sql = 'INSERT INTO custos (ccusto, descricao) VALUES (?,?)';
        let data = [custo.ccusto, custo.descricao];
        return db.executeSql(sql, data).catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  //Método p/ selecionar todos os recursos
  public selectAllCusto(filtro: string) {

    return this._database.createDataBase()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM custos WHERE ccusto LIKE ? OR descricao LIKE ?';
        let data = [filtro + '%', filtro + '%'];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let custos: any[] = [];
              for (let i = 0; i < data.rows.length; i++) {
                custos.push(data.rows.item(i));
              } // fim for()
              return custos;
            } // fim if()
            return [];
          }).catch((e) => console.error(e)); // fim db.executeSql()
      }).catch((e) => console.error(e)); // fim _database.createDataBase()
  }

}
