CREATE TABLE tb_app_eficiencia(
	codigo INTEGER NOT NULL IDENTITY PRIMARY KEY,
	data_inicial	VARCHAR(MAX) NOT NULL,
	hora_inicial	VARCHAR(MAX) NOT NULL,
	id_recurso		VARCHAR(MAX) NOT NULL,
	recurso			VARCHAR(MAX) NOT NULL,
	ccusto			VARCHAR(MAX) NOT NULL,
	id_operador		VARCHAR(MAX) NOT NULL,
	operador		VARCHAR(MAX) NOT NULL,
	medicao_inicial	INTEGER NOT NULL,
	usuario			VARCHAR(MAX) NOT NULL
)

CREATE TABLE TBAPPE(
    id INTEGER PRIMARY KEY IDENTITY NOT NULL,
    dt_ini VARCHAR(10) NOT NULL,
    hr_ini VARCHAR(8) NOT NULL,
    filial VARCHAR(2) NOT NULL,
    recurso VARCHAR(6) NOT NULL,
    produto VARCHAR(15) NULL,
    turno VARCHAR(1) NULL,
    turma VARCHAR(1) NULL,
    quantidade FLOAT NULL,
    resultado VARCHAR(7) NULL,
    medicao_ini FLOAT NULL,
    motivo VARCHAR(6) NULL,
    ccusto VARCHAR(9) NOT NULL,
    operador VARCHAR(6) NULL,
    usuario VARCHAR(6) NOT NULL,
    dt_import VARCHAR(10) NULL
 )