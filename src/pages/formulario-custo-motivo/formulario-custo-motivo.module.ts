import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormularioCustoMotivoPage } from './formulario-custo-motivo';

@NgModule({
  declarations: [
    FormularioCustoMotivoPage,
  ],
  imports: [
    IonicPageModule.forChild(FormularioCustoMotivoPage),
  ],
})
export class FormularioCustoMotivoPageModule {}
