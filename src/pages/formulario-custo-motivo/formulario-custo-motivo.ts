import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { CustoProvider } from '../../providers/custo/custo';
import { MotivoProvider } from '../../providers/motivo/motivo';

import { FormularioRecursosPage } from './../formulario-recursos/formulario-recursos';

@IonicPage()
@Component({
  selector: 'page-formulario-custo-motivo',
  templateUrl: 'formulario-custo-motivo.html',
})
export class FormularioCustoMotivoPage {

  //var relacionadas a custos
  private searchCusto;
  private aCustos : any[] = [];

  private searchMotivo;
  private aMotivos : any[] = [];
  private actvMotivo = false;


  private formGroup: FormGroup;

  private data = {
    dataHora: null,
    ccusto: null,
    motivo: null
  };

  constructor(
    private _navController: NavController,
    private _navParams: NavParams,
    private _formBuilder: FormBuilder,
    private _custoProvider: CustoProvider,
    private _motivoProvider: MotivoProvider) {

    this.formGroup = this._formBuilder.group({
      custo: ['', [Validators.required]],
      rCusto: ['', [Validators.required]],
      motivo: ['', [Validators.required]],
      rMotivo: ['', [Validators.required]],
    });
  }

  private ionViewWillEnter(): void {
    //console.log(JSON.stringify(this._navParams.data));
  }

  private filterCusto(event: any) {
    //console.log('Chamou o método filterCusto()');
    this.getCustoFiltered(this.searchCusto);
  }

  private getCustoFiltered(search): void {
    //console.log('Chamou o método getCustoFiltered()');
    if (search) {
      //console.log(search);
      this._custoProvider.selectAllCusto(search)
        .then((result: any[]) => this.aCustos = result);
    }
  }

  private filterMotivo(event: any) {
    this.getMotivoFiltered(this.searchMotivo);
  }

  private getMotivoFiltered(search): void {
    if (search) {
      //console.log(search, this.formGroup.value.rCusto);
      let split = this.formGroup.value.rCusto.split('-');
      this._motivoProvider.selectAllMotivo(search, split[0])
        .then((result: any[]) => this.aMotivos = result);
    }

    //console.log(JSON.stringify(this.aMotivos[0]));
  }

  private callPageRecursos(): void {
      this.data.dataHora = this._navParams.data;
      this.data.ccusto = this.formGroup.value.rCusto;
      this.data.motivo = this.formGroup.value.rMotivo;

      //console.log(JSON.stringify(this.data));
      this._navController.push(FormularioRecursosPage, this.data);
  }

  private activeMotivo(event: any): void {
    //console.log('chamado método activeMotivo()');
    this.actvMotivo = true;
  }
}
