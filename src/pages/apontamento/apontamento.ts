// import nativo
import { Component/*, ViewChild*/ } from '@angular/core';
import { Network } from '@ionic-native/network';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController, AlertController, LoadingController/*, Nav*/ } from 'ionic-angular';

// import de usuário
import { UserhttpProvider } from './../../providers/userhttp/userhttp';
import { ApontamentoProvider } from './../../providers/apontamento/apontamento';
//import { ConfiguracaoProvider } from './../../providers/configuracao/configuracao';
//================================================================================
import { Apontamento } from './../../model/apontamento.model';
//import { Configuracao } from './../../model/configuracao.model';
//================================================================================
import { EficienciaPage } from './../eficiencia/eficiencia';

@IonicPage()
@Component({
  selector: 'page-apontamento',
  templateUrl: 'apontamento.html',
})
export class ApontamentoPage {

  //@ViewChild(Nav) nav: Nav;

  //pages: Array<{title: string, component: any}>;

  private ip: string;
  private porta: string;

  private params;

  // Array de apontamentos, recebe dados do banco SQLite
  aApontamentos: any[] = [];

  // Armazena texto de pesquisa
  searchText: string = null;

  constructor(
    private _navController: NavController,
    private _navParams: NavParams,
    private _toastController: ToastController,
    private _alertController: AlertController,
    private _loadingController: LoadingController,
    private _network: Network,
    private _apontamentoProvider: ApontamentoProvider,
    private _userHttpProvider: UserhttpProvider,
  ) {

    /*
    this.pages = [
      { title: 'Eficiência Operacional', component: ApontamentoPage },
      { title: 'Eficiência Apontamento', component: EficienciaPage }
    ];
    */

  }

  ionViewDidEnter(): void {
    //this._navParams.data = this._navParams.data;
    //console.log(JSON.stringify(this._navParams.data));

    this.ip = this._navParams.data.ip;
    this.porta = this._navParams.data.porta;
  }


  // Pega todos os apontamentos do banco
  private getAllApontamentos(): void {
    this._apontamentoProvider.selectAllApontamento(this.searchText)
      .then((rs: any[]) => {
        // Limpa Array
        this.aApontamentos = [];
        this.aApontamentos = rs;
      });
  }

  // Chama o getAllApontamentos() antes de entrar na página
  ionViewWillEnter(): void {
    this.getAllApontamentos();
  }

  // Filtrar os apontamentos no input de pesquisa.
  private filter(ev: any): void {
    this.getAllApontamentos();
  }

  // Chama tela de apontamento de eficiência, passando o usuário logado por parâmetro
  private callPageEficiencia(): void {
    this._navController.push(EficienciaPage, this._navParams.data);
  }

  // Remove apontamento de eficiência do SQLite
  private delete(apontamento: Apontamento): void {
    this._apontamentoProvider.deleteApontamento(apontamento.codigo)
      .then(() => {
        //Removendo apontamento do array
        let index = this.aApontamentos.indexOf(apontamento);
        this.aApontamentos.splice(index, 1);

        //Mensagem
        this._toastController.create({
          message: 'Apontamento Removido',
          duration: 800,
          position: 'bottom'
        }).present();
      })
  }

  // Alertas **
  // Rede desconectada
  private showAlertDisconnected(): void {
    let alert = this._alertController.create({
      title: 'Erro de Conexão',
      subTitle: 'OPS...Verifique a sua conexão com a rede!',
      buttons: ['OK']
    });
    alert.present();
  }

  // Dados enviado/sincronizados
  private showAlertDataSend(): void {
    let alert = this._alertController.create({
      title: 'Apontamento',
      subTitle: 'Dados enviados com sucesso!',
      buttons: ['OK']
    });
    alert.present();

    //Limpa o array de apontamentos
    this.aApontamentos = [];
  }

  // Sem apontamentos
  private showAlertNotFindApontamentos(): void {
    let alert = this._alertController.create({
      title: 'Apontamento',
      subTitle: 'Não existem dados a serem enviados!',
      buttons: ['OK']
    });
    alert.present();
  }

  // Ddados não enviado
  private showAlertNotDataSend(): void {
    let alert = this._alertController.create({
      title: 'Erro ao enviar dados',
      subTitle: 'Falha na conexão com o servidor',
      buttons: ['OK']
    });
    alert.present();

    //Limpa o array de apontamentos
    this.aApontamentos = [];
  }
  // Fim Alertas **

  // Verifica se há conexão com a rede
  private isConnectedInNetwork(): boolean {
    if (this._network.type != 'none') {
      return true;
    }
    return false;
  }

  // Verifica se há apontamento a serem enviados
  /*
  private existApontamento(): boolean {
    if (this.aApontamentos.length > 0) {
      return true;
    }
    return false;
  }
  */

  // Envia os apontamentos via http com o userHttpProvider
  /*
  private send(): Array<any> {

    let isNotSend = false;
    let i;

    //Loading de carregamento
    let loader = this._loadingController.create({
      content: 'Sincronizando...',
    });

    //Código p/ enviar os dados
    loader.present().then(async () => {
      //Laço para preencher objeto JSON
      for (i = 0; i < this.aApontamentos.length; i++) {
        //Objeto JSON, recebe os dados do array de apontamento
        let apontamento = {
          "codigo": this.aApontamentos[i].codigo,
          "data_inicial": this.aApontamentos[i].data_inicial,
          "hora_inicial": this.aApontamentos[i].hora_inicial,
          "id_recurso": this.aApontamentos[i].id_recurso,
          "recurso": this.aApontamentos[i].recurso,
          "ccusto": this.aApontamentos[i].ccusto,
          "id_operador": this.aApontamentos[i].id_operador,
          "operador": this.aApontamentos[i].operador,
          "medicao_inicial": this.aApontamentos[i].medicao_inicial,
          "usuario": this.aApontamentos[i].usuario
        }

        // Enviando JSON 
        await this._userHttpProvider.postApontamento(apontamento)
          .then(async (Status) => {
            if (Status == '200') {
              console.log('Enviado com sucesso! Status -> ' + Status);

              // Deletando Apontamento do SQLite
              await this._apontamentoProvider.deleteApontamento(apontamento.codigo)
                //.then((x) => console.log(JSON.stringify(x)))
                .catch((Err) => console.log(Err));
            }
          }).catch((Err) => {
            if (Err == '500') {
              console.log('Falha ao conectar-se com o servidor!');
              isNotSend = true;
            }
          })

        // Se der erro ao enviar dados, para o laço FOR
        if (isNotSend) {
          break;
        }

      }
    });

    return [isNotSend, loader, i];
  }
  */

  // Confirma com alertas os apontamento enviados
  /*
  private confirmSend(data: Array<any>) {
    if (data[2] == this.aApontamentos.length && !data[0]) {
      data[1].dismiss().then(() => this.showAlertDataSend());
    } else {
      data[1].dismiss().then(() => this.showAlertNotDataSend());
    }
  }
  */

  // Responsável por validar envio e chamar:
  // isConnectedInNetwork(), existApontamento(), send(), confirmSend()
  private sincronizar(): void {
    if (this.isConnectedInNetwork()) {
      this.getAllApontamentos();

      if (this.aApontamentos.length > 0) {


        //Loading de carregamento
        let loader = this._loadingController.create({
          content: 'Sincronizando...',
        });

        //Código p/ enviar os dados
        loader.present().then(async () => {

          let isNotSend = false;
          let i;

          //Laço para preencher objeto JSON
          for (i = 0; i < this.aApontamentos.length; i++) {
            //Objeto JSON, recebe os dados do array de apontamento
            let apontamento = {
              "codigo": this.aApontamentos[i].codigo,
              "data_inicial": this.aApontamentos[i].data_inicial,
              "hora_inicial": this.aApontamentos[i].hora_inicial,
              "id_recurso": this.aApontamentos[i].id_recurso,
              "recurso": this.aApontamentos[i].recurso,
              "ccusto": this.aApontamentos[i].ccusto,
              "id_operador": this.aApontamentos[i].id_operador,
              "operador": this.aApontamentos[i].operador,
              "medicao_inicial": this.aApontamentos[i].medicao_inicial,
              "turma": this.aApontamentos[i].turma,
              "usuario": this.aApontamentos[i].usuario,
              "filial": this.aApontamentos[i].filial
            }

            //console.log(JSON.stringify(apontamento));


            // Enviando JSON 
            await this._userHttpProvider.postApontamento(apontamento, this.ip, this.porta)
              .then(async (Status) => {
                if (Status == '200') {
                  //console.log('Enviado com sucesso! Status -> ' + Status);

                  // Deletando Apontamento do SQLite
                  await this._apontamentoProvider.deleteApontamento(apontamento.codigo)
                    //.then((x) => console.log(JSON.stringify(x)))
                    .catch((Err) => console.log(Err));
                }
              }).catch((Err) => {
                if (Err == '500') {
                  //console.log('Falha ao conectar-se com o servidor!');
                  isNotSend = true;
                }
              })

            // Se der erro ao enviar dados, para o laço FOR
            if (isNotSend) {
              break;
            }

          }

          if (i == this.aApontamentos.length && !isNotSend) {
            //console.log("ENVIOU");

            loader.dismiss().then(() => this.showAlertDataSend());
          } else {
            //console.log("NÃO ENVIOU");
            loader.dismiss().then(() => this.showAlertNotDataSend());
          }

        });

      } else {
        this.showAlertNotFindApontamentos();
      }
    }
    else {
      this.showAlertDisconnected();
    }
  }
}
