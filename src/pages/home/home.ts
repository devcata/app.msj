// import nativo
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';

// import de usuário
import { Network } from '@ionic-native/network';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
//==================================================================
import { Usuario } from './../../model/usuario.model';
import { Configuracao } from './../../model/configuracao.model';
//import { Funcionario } from './../../model/funcionario.model';
//import { Recurso } from './../../model/recurso.model';
//==================================================================
import { UsuarioProvider } from './../../providers/usuario/usuario';
import { FuncionarioProvider } from './../../providers/funcionario/funcionario';
import { RecursoProvider } from './../../providers/recurso/recurso';
import { CustoProvider } from './../../providers/custo/custo';
import { MotivoProvider } from './../../providers/motivo/motivo';
import { ConfiguracaoProvider } from './../../providers/configuracao/configuracao';
import { UserhttpProvider } from './../../providers/userhttp/userhttp';
//==================================================================
import { UsuarioPage } from './../../pages/usuario/usuario';
import { MenuPage } from './../../pages/menu/menu';
//import { ApontamentoPage } from './../../pages/apontamento/apontamento';
// fim import de usuário


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private configuracao: Configuracao;

  // Objeto p/ armazenar usuário localizado no banco p/ validação de login
  private usuario: Usuario;

  // Arrays p/ armazenar dados vindo via Http - API **
  private aRecursos: any[];
  private aFuncionarios: any[];
  private aCustos: any[];
  private aMotivos: any[];

  private sendToDataBase = false;

  //Atributos do formulário
  private formLogin: FormGroup;

  constructor(
    //Injeções de dependências
    private _formBuilder: FormBuilder,
    private _navController: NavController,
    private _network: Network,
    private _alertController: AlertController,
    private _loadingController: LoadingController,
    private _usuarioProvider: UsuarioProvider,
    private _funcionarioProvider: FuncionarioProvider,
    private _recursoProvider: RecursoProvider,
    private _custoProvider: CustoProvider,
    private _motivoProvider: MotivoProvider,
    private _userHttpProvider: UserhttpProvider,
    private _configuracaoProvider: ConfiguracaoProvider
  ) {

    //Validação dos campos do formulário de login
    this.formLogin = this._formBuilder.group({
      login: ['', [Validators.required]],
      senha: ['', [Validators.required]]
    });

    this.usuario = new Usuario();
    this.configuracao = new Configuracao();

  }

  private ionViewWillEnter(): void {
    this._configuracaoProvider.getConfiguracao().then((rs: any) => this.configuracao = rs);
  }

  // Autentica usuário no APP
  private logar(): void {
    // Verifica se o usuario existe no SQLite utilizando os dados digitados no Formulário
    this.getUsuarioInSQLite(this.formLogin.value.login, this.formLogin.value.senha)
      .then(() => {

        // Se o usuário existir e tiver o perfil 'Admin', entra na tela de cadastro de usuários
        if ((this.usuario.getLogin() == this.formLogin.value.login) && (this.usuario.getSenha() == this.formLogin.value.senha) && (this.usuario.getPerfil() == 'admin')) {
          this._navController.push(UsuarioPage);
          this.formLogin.reset();
        }
        // Se o usuário existir e tiver o perfil 'User', carrega os recursos e funcionários via Http,
        // inseri no SQLite e abre a tela de apontamentos.
        else if ((this.usuario.getLogin() == this.formLogin.value.login) && (this.usuario.getSenha() == this.formLogin.value.senha) && (this.usuario.getPerfil() == 'user')) {

          if (this.isConnectedInNetwork()) {

            //Loading de carregamento
            let loader = this._loadingController.create({
              content: 'Carregando dados...\n\nAguarde!'
            });

            loader.present().then(async () => {

              // TODO
              await this.getRecursosInHttp()
                .then(() => {
                  //console.log(JSON.stringify(this.aRecursos));
                  this.sendToDataBase = true;
                }).catch(() => {
                  this.sendToDataBase = false;
                  loader.dismiss();
                  this.showAlertErrorLoadData();
                  //console.log('Recursos carregados\n');
                });
              
              if (this.sendToDataBase) {
                await this.getFuncionariosInHttp()
                  .then(() => {
                    //console.log(JSON.stringify(this.aFuncionarios));
                    this.sendToDataBase = true;
                  }).catch(() => {
                    this.sendToDataBase = false;
                    loader.dismiss();
                    this.showAlertErrorLoadData();

                  });
              }

              if (this.sendToDataBase) {
                await this.getCustosInHttp()
                  .then(() => {
                    //console.log(JSON.stringify(this.aCustos));
                    this.sendToDataBase = true;
                  }).catch(() => {
                    this.sendToDataBase = false;
                    loader.dismiss();
                    this.showAlertErrorLoadData();
                  });
              }
              
              if (this.sendToDataBase) {
                await this.getMotivosInHttp()
                  .then(() => {
                    //console.log(JSON.stringify(this.aMotivos));
                    this.sendToDataBase = true;
                  }).catch(() => {
                    this.sendToDataBase = false;
                    loader.dismiss();
                    this.showAlertErrorLoadData();
                  });
              }
              
              //console.log(this.sendToDataBase);

              await this.sendDataBase(() => {
                loader.dismiss();
                this._navController.setRoot(MenuPage,{user: this.formLogin.value.login});
              });
            });

          } else {
            this.showAlertDisconnected();
          }
        } else {
          this.showAlertLoginError();
          this.formLogin.reset();
        }

      }); // fim THEN getUsuarioInSQLite()
  }

  private async sendDataBase(callback) {

    
    if (this.sendToDataBase) {

      //loop p/ inserir os recursos no SQLite
      for (let i = 0; i <= this.aRecursos.length; i++) {
        if (this.aRecursos[i]) {
          let recurso = {
            filial: this.aRecursos[i].FILIAL,
            codigo: this.aRecursos[i].CODIGO,
            descricao: this.aRecursos[i].DESCRICAO,
            ccusto: this.aRecursos[i].CCUSTO
          };
          console.log(JSON.stringify(recurso));
          console.log('\n\n\n\n\n\n\n\n');
          await this._recursoProvider.insertRecurso(recurso);
        }
      }
      
      // loop p/ inserir os funcionários no SQLite
      for (let i = 0; i <= this.aFuncionarios.length; i++) {
        if (this.aFuncionarios[i]) {
          let funcionario = {
            filial: this.aFuncionarios[i].FILIAL,
            matricula: this.aFuncionarios[i].MATRICULA,
            nome: this.aFuncionarios[i].NOME
          }
          console.log(JSON.stringify(funcionario));
          console.log('\n\n\n\n\n\n\n\n');
          await this._funcionarioProvider.insertFuncionario(funcionario);
        }
      }
      
      // loop p/ inserir os custos no SQLite
      for (let i = 0; i <= this.aCustos.length; i++) {
        if (this.aCustos[i]) {
          
          let custo = {
            ccusto: this.aCustos[i].CCUSTO,
            descricao: this.aCustos[i].DESCRICAO
          };

          console.log(JSON.stringify(custo));
          console.log('\n\n\n\n\n\n\n\n');

          await this._custoProvider.insertCusto(custo);
        }
      }

      //loop p/ inserir os motivos no SQLite
      for (let i = 0; i <= this.aMotivos.length; i++) {
        if (this.aMotivos[i]) {
          let motivo = {
            filial: this.aMotivos[i].FILIAL,
            ccusto: this.aMotivos[i].CCUSTO,
            motivo: this.aMotivos[i].MOTIVO,
            descricao: this.aMotivos[i].DESCRICAO
          };
          console.log(JSON.stringify(motivo));
          console.log('\n\n\n\n\n\n\n\n');
          await this._motivoProvider.insertMotivo(motivo);
        }
      }

      callback();

    }

  }

  // Verifica se há conexão com a rede
  private isConnectedInNetwork(): boolean {
    if (this._network.type != 'none') {
      return true;
    }
    return false;
  }

  // Busca usuário p/ validação de login
  private getUsuarioInSQLite(login: string, senha: string) {
    //console.log(login, senha);
    return new Promise((resolve, reject) => {

      this._usuarioProvider.selectUsuario(login, senha)
        .then((rs: any) => {

          if (rs == false) {
            //this.showAlertErroLogin();
            //console.log(rs);
            resolve();
          }

          //console.log(rs);


          this.usuario.setId(rs.id);
          this.usuario.setLogin(rs.login);
          this.usuario.setSenha(rs.senha);
          this.usuario.setPerfil(rs.perfil);
          resolve();

        }).catch((e) => reject(e));
    });
  }

  // Busca os dados de "recursos" via Http 
  private getRecursosInHttp() {
    return new Promise((resolve, reject) => {

      this._userHttpProvider.getRecursos(this.configuracao.getIp(), this.configuracao.getPorta())
        .subscribe((data) => {
          this.aRecursos = data;
          resolve();
        },
        (error) => reject(error));
    });
  }

  // Busca os dados de "funcionarios" via Http 
  private getFuncionariosInHttp() {
    return new Promise((resolve, reject) => {

      this._userHttpProvider.getFuncionarios(this.configuracao.getIp(), this.configuracao.getPorta())
        .subscribe((data) => {
          this.aFuncionarios = data;
          resolve();
        },
        (error) => reject(error));
    });
  }

  // Busca os dados de "custos" via Http 
  private getCustosInHttp() {
    return new Promise((resolve, reject) => {

      this._userHttpProvider.getCustos(this.configuracao.getIp(), this.configuracao.getPorta())
        .subscribe((data) => {
          this.aCustos = data;
          resolve();
        },
        (error) => reject(error));
    });
  }

  // Busca os dados de "custos" via Http 
  private getMotivosInHttp() {
    return new Promise((resolve, reject) => {

      this._userHttpProvider.getMotivos(this.configuracao.getIp(), this.configuracao.getPorta())
        .subscribe((data) => {
          this.aMotivos = data;
          resolve();
        },
        (error) => reject(error));
    });
  }

  // Rede desconectada
  private showAlertDisconnected() {
    let alert = this._alertController.create({
      title: 'Erro de Conexão',
      subTitle: 'OPS...Verifique a sua conexão com a rede!',
      buttons: ['OK']
    });
    alert.present();
  }

  // Usuário ou senha inválido
  private showAlertLoginError() {
    let alert = this._alertController.create({
      title: 'Login Inválido',
      subTitle: 'Usuário e/ou senha inválido',
      buttons: ['OK']
    });
    alert.present();
  }

  // Erro ao carregar recursos ou funcionários
  private showAlertErrorLoadData() {
    let alert = this._alertController.create({
      title: 'Ops...',
      subTitle: 'Falha de conexão com o servidor!',
      buttons: ['OK']
    });
    alert.present();
  }

}