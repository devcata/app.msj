import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EficienciaPage } from './eficiencia';

@NgModule({
  declarations: [
    EficienciaPage,
  ],
  imports: [
    IonicPageModule.forChild(EficienciaPage),
  ],
})
export class EficienciaPageModule {}
