// import nativos
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// import de usuário
import { Recurso } from './../../model/recurso.model';
import { Apontamento } from './../../model/apontamento.model';
//================================================================
import { RecursoProvider } from './../../providers/recurso/recurso';
import { FuncionarioProvider } from './../../providers/funcionario/funcionario';
import { ApontamentoProvider } from './../../providers/apontamento/apontamento';

@IonicPage()
@Component({
  selector: 'page-eficiencia',
  templateUrl: 'eficiencia.html',
})
export class EficienciaPage {

  //Atributos do formulário
  private formGroup : FormGroup;

  //Variáveis de pesquisa vinculada aos input ion-searchbar
  private searchRec : string = null;
  private searchFun : string = null;
  private ApontamentoContinuo : string = 'Não';

  //Array de recursos e funcionarios, recebem dados do SQLite
  private aRecursos : any[] = [];
  private aFuncionarios : any[] = [];

  //Objeto de apontamento
  private apontamento : Apontamento;

    //Variável para armazenamento do usuário logado, recebe dado via navParams
  private usuarioLogado : string;

  constructor(
    private _navController : NavController, 
    private _navParams : NavParams,
    private _formBuilder : FormBuilder,
    private _toastController : ToastController,
    private _apontamentoProvider : ApontamentoProvider,
    private _recursoProvider : RecursoProvider,
    private _funcionarioProvider : FuncionarioProvider
  ) 
  {

    this.apontamento = new Apontamento();

    //Validação dos campos do formulário de login
    this.formGroup = this._formBuilder.group({
      //data_inicial: ['', [Validators.required]],
      //hora_inicial: ['', [Validators.required]],
      recurso: ['', [Validators.required]],
      operador: ['', [Validators.required]],
      radioFun: ['', [Validators.required]],
      radioRec: ['', [Validators.required]],
      medicaoInicial: ['', [Validators.required]],
      turma: ['', [Validators.required]]
      //modApontamento: ['', [Validators.required]]
    });
  }

  ionViewDidLoad() : void
  {
    this.usuarioLogado = this._navParams.data.user;

    /*
    if (this._navParams.data.user) {
      this.usuarioLogado = this._navParams.data.user;
    }
    else if (this._navParams.data.user.usuario) {
      this.usuarioLogado = this._navParams.data.user.usuario;
      //console.log("Eficienca page: " + this._navParams.data.id);
    }
    */
  }

  // Armazena o recurso filtrado no array
  private getRecursoFilter() : void
  {
    this._recursoProvider.selectAllRecurso(this.searchRec.toUpperCase())
    .then((rs: any[]) => {
      this.aRecursos = rs;
    });
  }

  // Método para o filtro de recursos via evento
  private filterRecursos(ev: any) : void
  {
    this.getRecursoFilter();
  }

  // Armazena o funcionário filtrado no array
  private getFuncionarioFilter() : void
  {
    if (this.searchFun) {
      this._funcionarioProvider.selectAllFuncionario(this.searchFun.toUpperCase())
      .then((rs: any[]) => {
        this.aFuncionarios = rs;
      });
    }
  }

  // Método para o filtro de funcionários via evento
  private filterFuncionarios(ev: any) : void
  {
    this.getFuncionarioFilter();
  }

  // Verifica se o controle de apontamento contínuo está ativado ou desativado
  private notify(event) : void
  {
    if (event.checked) {
      this.ApontamentoContinuo = 'Sim';
    }
    else {
      this.ApontamentoContinuo = 'Não';
    }
  }

  // Inseri os dados recebidos do formulário no SQLIte
  private saveApontamento() : Promise<any>
  {
    //this.apontamento.setDataInicial(this.formGroup.value.data_inicial);
    //this.apontamento.setHoraInicial(this.Form.value.hora_inicial);

    for (let i = 0; i <= this.aRecursos.length; i++) {

      if (this.aRecursos[i] && (this.aRecursos[i].codigo == this.formGroup.value.radioRec)) {

        this.apontamento.setIdRecurso(this.aRecursos[i].codigo);
        //console.log(this.apontamento.getIdRecurso());
        
        this.apontamento.setRecurso(this.aRecursos[i].descricao);
        //console.log(this.apontamento.getRecurso());
        
        this.apontamento.setCCusto(this.aRecursos[i].ccusto);
        //console.log(this.apontamento.getCCusto());
        
        
      }
    }

    for (let i = 0; i <= this.aFuncionarios.length; i++) {

      if (this.aFuncionarios[i] && (this.aFuncionarios[i].nome == this.formGroup.value.radioFun)) {

        this.apontamento.setIdOperador(this.aFuncionarios[i].matricula);
        //console.log(this.apontamento.getIdOperador());
        
        this.apontamento.setOperador(this.aFuncionarios[i].nome);
        //console.log(this.apontamento.getOperador());
        this.apontamento.setMedicaoInicial(this.formGroup.value.medicaoInicial);
        //console.log(this.apontamento.getMedicaoInicial());

        this.apontamento.setTurma(this.formGroup.value.turma);
        
        
      }
    }

    this.apontamento.setUsuario(this.usuarioLogado);
    this.searchFun = null;
    
    
    return this._apontamentoProvider.insertApontamento(this.apontamento);
  }

  // Salva um apontamento e aprensenta uma mensagem
  private apontar() : void
  {
    this.saveApontamento()
    .then(() => {
      this._toastController.create({
        message: 'Apontamento do funcionário ' + this.formGroup.value.radioFun + ' incluído!',
        duration: 2000,
        position: 'botton'
      }).present();


      if (this.ApontamentoContinuo == 'Não') {
        this._navController.pop();
      }
    })
    .catch(() => {
      this._toastController.create({
        message: 'Erro ao incluir apontamento.',
        duration: 3000,
        position: 'botton'
      }).present();
    })
  }

}