import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ConfiguracaoProvider } from './../../providers/configuracao/configuracao';

import { ApontamentoPage } from '../../pages/apontamento/apontamento';
import { ImprodutivosPage } from '../../pages/improdutivos/improdutivos';

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  private configure = {
    ip: null,
    porta: null,
    user: null
  };

  constructor(
    private _navController: NavController,
    private _navParams: NavParams,
    private _configuracaoProvider: ConfiguracaoProvider) {
  }

  private ionViewWillEnter(): void {

    this.configure.user = this._navParams.data.user;

    this._configuracaoProvider.getConfiguracao().then((rs: any) => {
      this.configure.ip = rs.ip;
      this.configure.porta = rs.porta;
      //console.log(JSON.stringify(this.configure));
    });

  }

  private callPageEficienciaOperacional(): void {
    //console.log(JSON.stringify(this.configure));
    this._navController.push(ApontamentoPage, this.configure);
  }

  private callPageHorasImprodutivas(): void {
    //console.log(JSON.stringify(this.configure));
    //console.log('callPageHorasImprodutivas');
    this._navController.push(ImprodutivosPage, this.configure);
  }

}
