// import nativo
import { Component } from '@angular/core';
import { IonicPage, NavController, /*NavParams,*/ ToastController } from 'ionic-angular';

// import de usuário
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
//==================================================================
import { ConfiguracaoProvider } from './../../providers/configuracao/configuracao';
//==================================================================
import { Configuracao } from './../../model/configuracao.model';

@IonicPage()
@Component({
  selector: 'page-configuracao',
  templateUrl: 'configuracao.html',
})
export class ConfiguracaoPage {

  private configuracao : Configuracao;
  private formGroup : FormGroup;

  constructor
  (
    private _navController : NavController, 
    //private _navParams : NavParams,
    private _toastController : ToastController,
    private _formBuilder : FormBuilder,
    private _configuracaoProvider : ConfiguracaoProvider
  ) 
  {

    this.configuracao = new Configuracao();

    // Validação dos campos do formulário
    this.formGroup = this._formBuilder.group({
      ip: ['', [Validators.required, Validators.pattern('^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$')]],
      porta: ['', [Validators.required, Validators.pattern('^([0-9]){1,5}')]]
    });

  }

  ionViewDidEnter() : void
  {
    this._configuracaoProvider.getConfiguracao().then((rs: any) => this.configuracao = rs);
  }

  private setConfiguracao() : Promise<any>
  {
    return this._configuracaoProvider.updateConfiguracao(this.configuracao);
  }

  private update() : void
  {
    this.setConfiguracao()
      .then(() => {
        this._toastController.create({
          message: 'Configuração atualizada.',
          duration: 3000,
          position: 'botton'
        }).present();
        this._navController.pop();
      })
      .catch(() => {
        this._toastController.create({
          message: 'Erro ao atualizar.',
          duration: 3000,
          position: 'botton'
        }).present();
    })
  }

}