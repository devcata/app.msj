import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { RecursoProvider } from '../../providers/recurso/recurso';
import { ImprodutivoProvider } from '../../providers/improdutivo/improdutivo';

import { ImprodutivosPage } from '../improdutivos/improdutivos';

@IonicPage()
@Component({
  selector: 'page-formulario-recursos',
  templateUrl: 'formulario-recursos.html',
})
export class FormularioRecursosPage {

  private formGroup: FormGroup;
  private searchRecurso;
  private aRecursos: any[] = [];
  private aObjRecursos: any[] = [];
  private cb_value: boolean;

  private data = {
    dataInicial: null,
    dataFinal: null,
    horaInicial: null,
    horaFinal: null,
    ccusto: null,
    getccusto: function(data) {
      let split = data.split('-');
      return split[0];
    },
    desccusto: null,
    getdesccusto: function(data) {
      let split = data.split('-');
      return split[1];
    },
    idMotivo: null,
    getIdMotivo: function (data) {
      let split = data.split('-');
      return split[0];
    },
    motivo: null,
    getMotivo: (data) => {
      let split = data.split('-');
      return split[1];
    },
    recursos: null
    /*
    formatDate: function (data) {
      if (data) {
        let split = data.split('-');
        return `${split[2]}/${split[1]}/${split[0]}`;
      }
      return '';
    },
    dataIniFormat: null,
    dataFimFormat: null
    */
  }

  constructor(
    private _navController: NavController,
    private _navParams: NavParams,
    private _formBuilder: FormBuilder,
    private _recursoProvider: RecursoProvider,
    private _improdutivoProvider: ImprodutivoProvider,
    private _loadingController: LoadingController,
    private _toastController: ToastController, ) {


    this.formGroup = this._formBuilder.group({
      recurso: ['', [Validators.required]]
      //cbRecurso: ['', [Validators.required]]
    });

  }

  private ionViewWillEnter(): void {

    this.data.dataInicial = this._navParams.data.dataHora.dataInicial;
    this.data.dataFinal = this._navParams.data.dataHora.dataFinal;
    this.data.horaInicial = this._navParams.data.dataHora.horaInicial;
    this.data.horaFinal = this._navParams.data.dataHora.horaFinal;
    this.data.ccusto = this.data.getccusto(this._navParams.data.ccusto);
    this.data.desccusto = this.data.getdesccusto(this._navParams.data.ccusto);
    this.data.idMotivo = this.data.getIdMotivo(this._navParams.data.motivo);
    this.data.motivo = this.data.getMotivo(this._navParams.data.motivo);
    //this.data.dataIniFormat = this.data.formatDate(this._navParams.data.dataHora.dataInicial);
    //this.data.dataFimFormat = this.data.formatDate(this._navParams.data.dataHora.dataFinal);

    console.log(JSON.stringify(this.data));

  }

  private filterRecurso(event: any): void {
    this.getRecursosFiltered(this.searchRecurso);
  }

  private getRecursosFiltered(search) {
    //console.log('Chamou o método getCustoFiltered()');
    if (search) {
      //console.log(search);
      this._recursoProvider.selectAllRecurso(search)
        .then((result: any[]) => this.aRecursos = result);
    }
  }

  private getCbRecurso(recurso: any): void {
    if (this.aObjRecursos.indexOf(recurso) === -1) {
      this.aObjRecursos.push(recurso);
    } else {
      this.aObjRecursos.splice(this.aObjRecursos.indexOf(recurso), 1);
    }
    //console.log(JSON.stringify(this.aObjRecursos));
  }

  private async apontar() {

    this.data.recursos = JSON.parse(JSON.stringify(this.aObjRecursos));
    
    if (this.data.recursos.length > 0) {
      await this._improdutivoProvider.insertImpro_A(this.data);
      for (let i = 0; i < this.data.recursos.length; i++) {
        await this._improdutivoProvider.insertImpro_B(this.data.recursos[i]);
      }
      this._toastController.create({
        message: 'Apontado com sucesso',
        duration: 2000,
        position: 'botton'
      }).present()
          .then(() => this._navController.popTo(this._navController.getByIndex(this._navController.length()-4)));
    } else {
      this._toastController.create({
        message: ' Apontameto improdutivo sem recurso selecionado',
        duration: 2000,
        position: 'botton'
      }).present();
    }
  }

}