import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormularioRecursosPage } from './formulario-recursos';

@NgModule({
  declarations: [
    FormularioRecursosPage,
  ],
  imports: [
    IonicPageModule.forChild(FormularioRecursosPage),
  ],
})
export class FormularioRecursosPageModule {}
