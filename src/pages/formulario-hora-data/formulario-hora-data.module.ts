import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormularioHoraDataPage } from './formulario-hora-data';

@NgModule({
  declarations: [
    FormularioHoraDataPage,
  ],
  imports: [
    IonicPageModule.forChild(FormularioHoraDataPage),
  ],
})
export class FormularioHoraDataPageModule {}
