import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import moment from 'moment';


import { FormularioCustoMotivoPage } from './../formulario-custo-motivo/formulario-custo-motivo';

@IonicPage()
@Component({
  selector: 'page-formulario-hora-data',
  templateUrl: 'formulario-hora-data.html',
})
export class FormularioHoraDataPage {

  private formGroup : FormGroup;

  private data = {
    dataInicial: null,
    dataFinal: null,
    horaInicial: null,
    horaFinal: null 
  }

  constructor(
    private _navController: NavController, 
    private _navParams: NavParams,
    private _formBuilder : FormBuilder) {

      //Validação dos campos do formulário de login
    this.formGroup = this._formBuilder.group({
      dataInicial: ['', [Validators.required]],
      dataFinal: [''],
      horaInicial: ['', [Validators.required]],
      horaFinal: ['', ]
    });
  }

  private ionViewWillEnter(): void {

    let data = moment().format('YYYY-MM-DD');
    let hora = moment().format('HH:mm');

    //console.log(data);
    //console.log(hora);

    this.data.dataInicial = data;
    this.data.horaInicial = hora;

  }

  private callPageCustoMotivo(): void {
    
    this.data.dataInicial = this.formGroup.value.dataInicial;
    this.data.dataFinal = this.formGroup.value.dataFinal;
    this.data.horaInicial = this.formGroup.value.horaInicial;
    this.data.horaFinal = this.formGroup.value.horaFinal;

    console.log(JSON.stringify(this.data));
    this._navController.push(FormularioCustoMotivoPage, this.data);
  }


}
