import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ImprodutivoProvider } from '../../providers/improdutivo/improdutivo';

import { ImprodutivosPage } from '../improdutivos/improdutivos';

@IonicPage()
@Component({
  selector: 'page-data-hora-update',
  templateUrl: 'data-hora-update.html',
})
export class DataHoraUpdatePage {

  private formGroup: FormGroup;
  private aImpro_B: any [] = [];

  constructor(
    private _navParams: NavParams,
    private _navController: NavController,
    private _improdutivoProvider: ImprodutivoProvider,
    private _toastController: ToastController,
    private _formBuilder: FormBuilder, ) {

    //Validação dos campos do formulário de login
    this.formGroup = this._formBuilder.group({
      dataFinal: ['', [Validators.required]],
      horaFinal: ['', [Validators.required]]
    });

  }

  ionViewWillEnter() {
    console.log(JSON.stringify(this._navParams.data));
    this._improdutivoProvider.selectAllImpro_B(this._navParams.data.id)
      .then((resB: any[]) => {
        this.aImpro_B = [];
        this.aImpro_B = resB;
        console.log(JSON.stringify(this.aImpro_B));
      })
  }

  private update() {

    //console.log(this.formGroup.value.dataFinal);
    //console.log(this.formGroup.value.horaFinal);

    this._improdutivoProvider.updateImpro_A(this.formGroup.value.dataFinal, this.formGroup.value.horaFinal, this._navParams.data.id)
      .then(() => {
        this._toastController.create({
          message: 'Improdutivo atualizado',
          duration: 2000,
          position: 'botton'
        }).present()
          .then(() => this._navController.pop());
      })
      .catch(() => {
        this._toastController.create({
          message: 'Falha ao atualizar',
          duration: 2000,
          position: 'botton'
        }).present()
      })
  }
}
