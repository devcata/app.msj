import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataHoraUpdatePage } from './data-hora-update';

@NgModule({
  declarations: [
    DataHoraUpdatePage,
  ],
  imports: [
    IonicPageModule.forChild(DataHoraUpdatePage),
  ],
})
export class DataHoraUpdatePageModule {}
