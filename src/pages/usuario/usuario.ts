// import nativo
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

// import de usuário
import { UsuarioProvider } from '../../providers/usuario/usuario';
//================================================================
import { Usuario } from './../../model/usuario.model';
//================================================================
import { ConfiguracaoPage } from './../configuracao/configuracao';
import { UpdateUsuarioPage } from '../update-usuario/update-usuario';


@IonicPage()
@Component({
  selector: 'page-usuario',
  templateUrl: 'usuario.html',
})
export class UsuarioPage {

  //Array p/ armazenar usuários
  private aUsuarios: any[] = [];

  //String de pesquisa
  searchText: string = null;

  constructor(
    private _navController : NavController, 
    private _navParams : NavParams, 
    private _toastController : ToastController,
    private _usuarioProvider : UsuarioProvider
  ) {}

  // Pega todos usuários no SQLite e armazena no array
  private getAllUser() 
  {
    this._usuarioProvider.selectAllUsuario(this.searchText)
      .then((result: any[]) => {        
        this.aUsuarios = result;
      });
  }

  //Evento automática para carregar usuarios no array quando entrar na tela
  ionViewDidEnter() 
  {
    this.getAllUser();
  }

  // filtra usuário no banco e gravar no array
  private filterUser(ev : any) 
  {
    this.getAllUser();
  }

  // Remover selecionado
  private removeUser(usuario : Usuario) 
  {
    this._usuarioProvider.deleteUsuario(usuario.getId())
    .then(() => {
      //Removendo do array
      let index = this.aUsuarios.indexOf(usuario);
      this.aUsuarios.splice(index, 1);
      this._toastController.create({
        message: 'Usuário Removido',
        duration: 3000,
        position: 'bottom'
      }).present();
    })
  }

  // Chama a tela de adição de usuário 
  private callPageAddUser() : void 
  {
    this._navController.push(UpdateUsuarioPage);
  }

  // Chama a tela de adição de usuário passando o ID
  private callPageEditUser(id : number) : void 
  {
    this._navController.push(UpdateUsuarioPage, {id: id})
  }

  // Chama a tela de configuração 
  private callPageSetup():void {
    this._navController.push(ConfiguracaoPage);
  }
}
