import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImprodutivosPage } from './improdutivos';

@NgModule({
  declarations: [
    ImprodutivosPage
  ],
  imports: [
    IonicPageModule.forChild(ImprodutivosPage),
  ],
})
export class ImprodutivosPageModule {}