import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { FormularioHoraDataPage } from './../formulario-hora-data/formulario-hora-data';
import { DataHoraUpdatePage } from './../data-hora-update/data-hora-update';

import { ImprodutivoProvider } from '../../providers/improdutivo/improdutivo';
import { UserhttpProvider } from '../../providers/userhttp/userhttp';

@IonicPage()
@Component({
  selector: 'page-improdutivos',
  templateUrl: 'improdutivos.html',
})
export class ImprodutivosPage {

  private notSend = false;
  private Send = false;
  private aImpro_A: any[] = [];
  private aImpro_B: any[] = [];
  private aImprodutivoEnviar: any[] = [];

  private data = {
    impro_A: null,
    dataInicial: null,
    dataFinal: null,
    horaInicial: null,
    horaFinal: null,
    ccusto: null,
    idmotivo: null,
    impro_B: null,
    recurso: null,
    usuario: null
  }

  constructor(
    private _navController: NavController,
    private _navParams: NavParams,
    private _improdutivoProvider: ImprodutivoProvider,
    private _network: Network,
    private _alertController: AlertController,
    private _loadingController: LoadingController,
    private _userHttpProvider: UserhttpProvider,
    private _toastController: ToastController) { }

  private callPageFormHoraData(): void {
    this._navController.push(FormularioHoraDataPage);
  }

  ionViewWillEnter(): void {
    this.getAllImpro_A();
    console.log(JSON.stringify(this._navParams.data));
  }

  private getAllImpro_A(): void {
    this._improdutivoProvider.selectAllImpro_A()
      .then(async (resA: any[]) => {
        this.aImpro_A = [];
        this.aImpro_A = resA;
        //console.log(JSON.stringify(this.aImpro_A));
      })
  }

  private showImprodutivos(): void {
    //console.log(JSON.stringify(this.aImprodutivoEnviar))
  }

  private updateImproA(obj): void {
    this._navController.push(DataHoraUpdatePage, obj)
  }

  private delete(obj): void {
    this._improdutivoProvider.deleteImpro_A(obj.id)
      .then(() => {
        this._improdutivoProvider.deleteImpro_B(obj.id)
          .then(() => this.getAllImpro_A())
      })
  }

  // Verifica se há conexão com a rede
  private isConnectedInNetwork(): boolean {
    if (this._network.type != 'none') {
      return true;
    }
    return false;
  }

  private showAlertDisconnected(): void {
    let alert = this._alertController.create({
      title: 'Erro de Conexão',
      subTitle: 'OPS...Verifique a sua conexão com a rede!',
      buttons: ['OK']
    });
    alert.present();
  }

  // Sem apontamentos
  private showAlertNotFindApontamentos(): void {
    let alert = this._alertController.create({
      title: 'Horas Improdutivas',
      subTitle: 'Não existem dados a serem sincronizados!',
      buttons: ['OK']
    });
    alert.present();
  }

  // Ddados não enviado
  private showAlertNotDataSend(): void {
    let alert = this._alertController.create({
      title: 'Erro ao sincronizar dados',
      subTitle: 'Falha na conexão com o servidor',
      buttons: ['OK']
    });
    alert.present();

    //Limpa o array de apontamentos
    //this.aApontamentos = [];
  }

  private showAlertPending(): void {
    let alert = this._alertController.create({
      title: 'Alerta',
      subTitle: 'Existe apontamento com campo (data/hora final) vazio que não pode ser sincronizado',
      buttons: ['OK']
    });
    alert.present();
  }

  private synchronize(): void {

    if (this.isConnectedInNetwork()) {
      this.getAllImpro_A();
      this.synchronizing();
    } else {
      this.showAlertDisconnected();
    }

  }

  private async synchronizing() {

    //Loading de carregamento
    let loader = this._loadingController.create({
      content: 'Sincronizando...',
    });

    this.notSend = false;
    this.Send = false;

    loader.present();

    if (this.aImpro_A.length > 0) {
      console.log('existe dados');
      for (let i = 0; i < this.aImpro_A.length; i++) {
        if (!this.aImpro_A[i].dataFinal && !this.aImpro_A[i].horaFinal) {
          this.notSend = true;
          console.log('Não pode ENVIAR');
        }
        else {
          // Faz um select no impro_B de acordo com o ID do impro_A
          this.Send = true;
          await this._improdutivoProvider.selectAllImpro_B(this.aImpro_A[i].id)
            .then(async (resB: any[]) => {
              this.aImpro_B = [];
              this.aImpro_B = resB;

              //Prepara objeto para enviar
              for (let y = 0; y < this.aImpro_B.length; y++) {

                this.data.impro_A = this.aImpro_A[i].id,
                  this.data.dataInicial = this.aImpro_A[i].dataInicial,
                  this.data.dataFinal = this.aImpro_A[i].dataFinal,
                  this.data.horaInicial = this.aImpro_A[i].horaInicial,
                  this.data.horaFinal = this.aImpro_A[i].horaFinal,
                  this.data.ccusto = this.aImpro_A[i].ccusto,
                  this.data.idmotivo = this.aImpro_A[i].idmotivo,
                  this.data.impro_B = this.aImpro_B[y].id,
                  this.data.recurso = this.aImpro_B[y].recurso,
                  this.data.usuario = this._navParams.data.user

                await this._userHttpProvider.postImprodutivo(this.data, this._navParams.data.ip, this._navParams.data.porta)
                  .then(async () => {
                    await this._improdutivoProvider.deleteImpro_A(this.aImpro_A[i].id)
                      .then(async () => await this._improdutivoProvider.deleteImpro_B(this.aImpro_B[y].id).catch(() => console.log('Erro ao deletar impro_B')))
                      .then(() => {
                        let toast = this._toastController.create({
                          message: 'Sincronizado!',
                          duration: 1000,
                          position: 'botton'
                        }).present();
                      })
                      .catch(() => console.log('Erro ao deletar impro_A'))
                  })
                  .catch(async (err) => {
                    //console.log('CAINDO NO CATCH');
                    console.log(JSON.stringify(err));
                    this.notSend = false;
                    this.Send = false;
                    await loader.dismiss();
                    this.showAlertNotDataSend();
                  })

              }
            }) // Fim Then
        }
      }
    } else { // não possui dados para enviar
      await loader.dismiss();
      this.notSend = false;
      this.Send = false;
      this.showAlertNotFindApontamentos();
    }

    console.log('this.notSend: ' + this.notSend);
    console.log('this.Send: ' + this.Send);

    if (this.notSend || this.Send) {
      await loader.dismiss();
      this.getAllImpro_A();
      if (this.notSend) {
        this.showAlertPending()
      }
    }

  }
}