// import nativo
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

// import de usuário
import { UsuarioProvider } from './../../providers/usuario/usuario';
//==================================================================
import { Usuario } from './../../model/usuario.model';

@IonicPage()
@Component({
  selector: 'page-update-usuario',
  templateUrl: 'update-usuario.html',
})
export class UpdateUsuarioPage {

  private usuario : Usuario;

  private formGroup : FormGroup;

  constructor(
    private _navController : NavController, 
    private _navParams : NavParams,
    private _toastController : ToastController,
    private _formBuilder : FormBuilder,
    private _usuarioProvider : UsuarioProvider
  ) 
  {
    this.usuario = new Usuario();

    // Seleciona o usuário do banco pelo ID
    if (this._navParams.data.id) {
      this._usuarioProvider.selectUsuarioByID(this._navParams.data.id)
        .then((rs: any) => {
          this.usuario = rs;
        })
    }

    // Validação dos campos do formulário
    this.formGroup = this._formBuilder.group({
      usuario: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
      senha: ['', [Validators.required]],
      perfil: ['', [Validators.required]]
    });
  }

  // Salva ou edita usuário no SQLite
  private saveUsuario() : Promise<any>
  {
    if (this.usuario.getId()) {
      return this._usuarioProvider.updateUsuario(this.usuario);
    }
    else {
      return this._usuarioProvider.insertUsuario(this.usuario);
    }
  }

  // Chama o método de saveUsuario()
  private update() : void
  {
    this.saveUsuario()
      .then(() => {
        this._toastController.create({
          message: 'Usuário salvo.',
          duration: 3000,
          position: 'botton'
        }).present();
        this._navController.pop();
      })
      .catch(() => {
        this._toastController.create({
          message: 'Erro ao salvar usuário.',
          duration: 3000,
          position: 'botton'
        }).present();
      })
  }

}
