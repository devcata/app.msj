// import nativo
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
//===========================Outros=====================================
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { SQLite } from '@ionic-native/sqlite';

// import de usuário
//===========================Page=======================================
import { HomePage } from '../pages/home/home';
import { UsuarioPage } from '../pages/usuario/usuario';
import { ConfiguracaoPage } from '../pages/configuracao/configuracao';
import { UpdateUsuarioPage } from '../pages/update-usuario/update-usuario';
import { ApontamentoPage } from '../pages/apontamento/apontamento';
import { EficienciaPage } from '../pages/eficiencia/eficiencia';
import { MenuPage } from '../pages/menu/menu';
import { ImprodutivosPage } from '../pages/improdutivos/improdutivos';
import { FormularioHoraDataPage } from '../pages/formulario-hora-data/formulario-hora-data';
import { FormularioCustoMotivoPage } from '../pages/formulario-custo-motivo/formulario-custo-motivo';
import { FormularioRecursosPage } from '../pages/formulario-recursos/formulario-recursos';
import { DataHoraUpdatePage } from '../pages/data-hora-update/data-hora-update';
//=========================Provider=====================================
import { DatabaseProvider } from '../providers/database/database';
import { UserhttpProvider } from '../providers/userhttp/userhttp';
import { FuncionarioProvider } from '../providers/funcionario/funcionario';
import { RecursoProvider } from '../providers/recurso/recurso';
import { ConfiguracaoProvider } from '../providers/configuracao/configuracao';
import { ApontamentoProvider } from '../providers/apontamento/apontamento';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { CustoProvider } from '../providers/custo/custo';
import { MotivoProvider } from '../providers/motivo/motivo';
import { ImprodutivoProvider } from '../providers/improdutivo/improdutivo';
// Fim import de usuário

import { MyApp } from './app.component';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    UsuarioPage,
    ConfiguracaoPage,
    UpdateUsuarioPage,
    ApontamentoPage,
    EficienciaPage,
    MenuPage,
    ImprodutivosPage,
    FormularioHoraDataPage,
    FormularioCustoMotivoPage,
    FormularioRecursosPage,
    DataHoraUpdatePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    UsuarioPage,
    ConfiguracaoPage,
    UpdateUsuarioPage,
    ApontamentoPage,
    EficienciaPage,
    MenuPage,
    ImprodutivosPage,
    FormularioHoraDataPage,
    FormularioCustoMotivoPage,
    FormularioRecursosPage,
    DataHoraUpdatePage
  ],
  providers: [
    DatabaseProvider,
    UserhttpProvider,
    FuncionarioProvider,
    RecursoProvider,
    ConfiguracaoProvider,
    ApontamentoProvider,
    UsuarioProvider,
    CustoProvider,
    MotivoProvider,
    ImprodutivoProvider,
    StatusBar,
    SplashScreen,
    Network,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: LOCALE_ID, useValue: 'pt-BR'}
  ]
})
export class AppModule {}
