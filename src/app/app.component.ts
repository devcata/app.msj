// import nativos
import { Component, /*ViewChild*/ } from '@angular/core';
import { /*Nav,*/ Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


// import de usuário
import { HomePage } from '../pages/home/home';
//import { ApontamentoPage } from '../pages/apontamento/apontamento';
//==================================================================
import { DatabaseProvider } from './../providers/database/database';
// fim import de usuário

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  //@ViewChild(Nav) nav: Nav;

  rootPage: any = null;
  //pages: Array<{title: string, component: any}>;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    database: DatabaseProvider
  ) {

    platform.ready().then(() => {

      statusBar.styleDefault();
      splashScreen.hide();

      //Inicializa a criação da base de dados, tabelas e usuário admin
      database.create()
        .then(() => {
          this.openHomePage(splashScreen);
        })
        .catch(() => {
          this.openHomePage(splashScreen);
        })

    });
  }

  private openHomePage(splashScreen: SplashScreen): void {
    splashScreen.hide();

    //Tela principal a ser aberta;
    this.rootPage = HomePage
  }
}