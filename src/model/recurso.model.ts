export class Recurso {

    private id: number;
    private codigo: string;
    private filial: string;
    private descricao: string;
    private ccusto: string;

    constructor() { }

	public getId(): number {
		return this.id;
	}

	public setId(value: number) {
		this.id = value;
	}
 

	public getCodigo(): string {
		return this.codigo;
	}

	public setCodigo(value: string) {
		this.codigo = value;
	}

	public getFilial(): string {
		return this.filial;
	}

	public setFilial(value: string) {
		this.filial = value;
	}
    

	public getDescricao(): string {
		return this.descricao;
	}

	public setDescricao(value: string) {
		this.descricao = value;
	}
    

	public getcCusto(): string {
		return this.ccusto;
	}

	public setcCusto(value: string) {
		this.ccusto = value;
	}
}