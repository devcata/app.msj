export class Apontamento 
{
    public codigo: number;
    public dataInicial: string;
    public horaInicial: string;
    public descricao: string;
    public idRecurso: string;
    public recurso: string;
    public cCusto: string;
    public idOperador: string;
    public operador: string;
    public medicaoInicial: string;
    public turma: string;
    public usuario: string;
    public filial: string;

    constructor() { }

    public getCodigo() : number
    {
        return this.codigo;
    }

    public getDataInicial() : string
    {
        return this.dataInicial;
    }

    public getHoraInicial() : string
    {
        return this.horaInicial;
    }

    public getDescricao() : string
    {
        return this.descricao;
    }

    public getIdRecurso() : string
    {
        return this.idRecurso;
    }

    public getRecurso() : string
    {
        return this.recurso;
    }

    public getCCusto() : string 
    {
        return this.cCusto;
    }

    public getIdOperador() : string 
    {
        return this.idOperador;
    }

    public getOperador() : string
    {
        return this.operador;
    }

    public getMedicaoInicial() : string 
    {
        return this.medicaoInicial;
    }

    public getUsuario() : string
    {
        return this.usuario;
    }

    public getTurma(): string {
        return this.turma;
    }

    public getFilial() : string
    {
        return this.filial;
    }

    public setCodigo(value : number)
    {
        this.codigo = value;
    }

    public setDataInicial(value : string)
    {
        this.dataInicial = value;
    }

    public setHoraInicial(value : string)
    {
        this.horaInicial = value;
    }

    public setDescricao(value : string)
    {
        this.descricao = value;
    }

    public setIdRecurso(value : string)
    {
        this.idRecurso = value;
    }

    public setRecurso(value : string)
    {
        this.recurso = value;
    }

    public setCCusto(value : string)
    {
        this.cCusto = value;
    }

    public setIdOperador(value : string)
    {
        this.idOperador = value;
    }

    public setOperador(value : string)
    {
        this.operador = value;
    }

    public setMedicaoInicial(value : string)
    {
        this.medicaoInicial = value;
    }

    public setTurma(value: string): void {
        this.turma = value;
    }

    public setUsuario(value : string)
    {
        this.usuario = value;
    }

    public setFilial(value : string)
    {
        this.filial = value;
    }

}