export class Usuario {

    public id: number;
    public login: string;
    public senha: string;
    public perfil: string;

    constructor() { }


	public getId(): number {
		return this.id;
	}

	public setId(value: number) {
		this.id = value;
	}
    
	public getLogin(): string {
		return this.login;
	}

	public setLogin(value: string) {
		this.login = value;
	}

	public getSenha(): string {
		return this.senha;
	}

	public setSenha(value: string) {
		this.senha = value;
	}

	public getPerfil(): string {
		return this.perfil;
	}

	public setPerfil(value: string) {
		this.perfil = value;
	}
    

}