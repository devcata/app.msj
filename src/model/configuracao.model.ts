export class Configuracao {
    
    public codigo: number;
    public ip: string;
    public porta: string;
  
    constructor() { }
  
	public getCodigo(): number {
		return this.codigo;
	}

	public setCodigo(value: number) {
		this.codigo = value;
	}

	public getIp(): string {
		return this.ip;
	}

	public setIp(value: string) {
		this.ip = value;
	}

	public getPorta(): string {
		return this.porta;
	}

	public setPorta(value: string) {
		this.porta = value;
	}
    
  }