export class Funcionario {

    private codigo: number;
    private nome: string;
    private matricula: string;
    private filial: string;
    
    constructor() { }

    public getCodigo() : number
    {
        return this.codigo;
    }

    public getNome() : string
    {
        return this.nome;
    }

    public getMatricula() : string
    {
        return this.matricula;
    }

    public getFilial() : string
    {
        return this.filial;
    }

    public setCodigo(value : number)
    {
        this.codigo = value;
    }

    public setNome(value : string)
    {
        this.nome = value;
    }

    public setMatricula(value : string)
    {
        this.matricula = value;
    }

    public setFilial(value : string)
    {
        this.filial = value;
    }
  
  }