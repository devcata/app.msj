export class StringBuilder
{
    private strings : any;

    constructor(value? : any)
    {
        this.strings = new Array("");
        this.append(value);
    }

    public append(value : any)
    {
        if (value)
        {
            this.strings.push(value);
        }
    }

    public clear()
    {
        this.strings.length = 1;
    }

    public toString()
    {
        return this.strings.join("");
    }
}